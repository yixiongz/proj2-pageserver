# Proj2-pageserver #

-author: Eric Zhou

-contact address email: yixiongz@uoregon.edu

-description: A local pageserver built in a Docker container, 
            

            if the request page exists in local,
              - send the content of the request page to server.
              
            otherwise, send the relevant status.
              - 404, if page not found
              - 403, if the name of the page is illegal
              - 401, if the file is not implemented.
