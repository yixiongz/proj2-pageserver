from flask import Flask, render_template, request

app = Flask(__name__)


@app.route("/")
def hello():
    return "UOCIS docker demo!"


@app.route('/', defaults={'path': ''})
@app.route('/<path:name>')
	# to get the path of the input by using path data type in Flask.
def respond(name=None):
	#name = request.args.get("name") 
	ans = name.split('/')
	
	if ans[-1][0] == '~' or ans[-1][0:2] == '..' or name.find("//") != -1:
		return	error_403(403)             
    
	else:
		if name[-5:] == ".html" or name[-4:] == ".css":
			try:
				return render_template(name, name = name) 
			except:
				return error_404(404)
		else:
			return error_401(401)


@app.errorhandler(404)
def error_404(name=None):
	return render_template('404.html', name = name), 404

@app.errorhandler(403)
def error_403(name=None):
	return render_template('403.html', name = name), 403

@app.errorhandler(401)
def error_401(name=None):
	return render_template('401.html', name = name), 401





if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')

"""
Reference:
	http://flask.pocoo.org/snippets/57/
	http://flask.pocoo.org/docs/1.0/quickstart/#quickstart

"""
